import config from "./config"
const debug = require('debug')('staircase')

interface CalculateStepsOptions {
  stepsPerStride: number
}

export class Staircase {
  private flights: number[];

  public constructor(flights: number[]) {
      this.flights = flights;
  }

  public calculateSteps(options: CalculateStepsOptions): number {
    return this.flights
      .reduce((acc, steps, index) => {
        const fullStrides = Math.floor(steps / options.stepsPerStride)
        const lastStride = (steps % options.stepsPerStride) > 0 ? 1 : 0
        const landingStrides = (index === 0 ? 0 : config.landing.stridesToTurn)
        const result =  acc + fullStrides + lastStride + landingStrides
        debug({
          steps: steps,
          fullStrides: fullStrides,
          lastStride: lastStride,
          landingStrides: landingStrides,
          result: result
        })
        return result
      }, 0)
  }
}

import * as readline from 'readline'
import { Validator, ValidationError} from './validator'

const readlineInterface = readline.createInterface(process.stdin, process.stdout);

export class Interrogator {

  public static async askAgain(): Promise<boolean> {
    try {
      const yesNo: string = await this.askQuestion('Restart? (y/n)')
      return Validator.parseRestartResponse(yesNo)
    } catch(err){
      this.prettyPrintError(err)
      return this.askAgain()
    }
  }

  public static async askStaircase(): Promise<number[]> {
    try {
      const staircaseJson: string = await this.askQuestion('Please insert the staircase definition as an array (e.g. [15, 18])')
      return Validator.parseAndValidateStaircase(staircaseJson)
    } catch(err){
      this.prettyPrintError(err)
      return this.askStaircase()
    }
  }

  public static async askStepsPerStride(): Promise<number> {
    try {
      const answer = await this.askQuestion('How many steps can you do per stride?')
      return Validator.parseAndValidateStepsPerStride(answer)
    } catch(err){
      this.prettyPrintError(err)
      return this.askStepsPerStride()
    }
  }

  private static async askQuestion(question: string): Promise<string> {
    return new Promise((resolve, reject) => {
      console.log(`\n${question}`)
      readlineInterface.on('line', (lineRaw: string) => {
          const line = lineRaw.trim()
          resolve(line)
      }).on('close', () =>{
          reject('User interruption')
      });
    })
  }

  private static prettyPrintError(err: ValidationError): void {
    const formatted = err.message
      .split('\n')
      .map((el, index) => index === 0 ? el : '     > ' + el)
      .join('\n')
    console.error(`[Illegal User input] ${formatted}`)
  }
}

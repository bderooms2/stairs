import config from "./config"

export class ValidationError extends Error {
    public constructor(m: string) {
        super(m);
        Object.setPrototypeOf(this, ValidationError.prototype);
    }
}

export class Validator {
  public static parseAndValidateStaircase(input: string): number[] {
    const json = this.parseJson(input)
    this.isValidArray(json)
    this.onlyNumbersInArray(json)
    this.hasValidLength(json)
    this.hasValidFlightLength(json)
    return json as number[]
  }

  public static parseAndValidateStepsPerStride(input: string): number {
    const stepsPerStride = this.parseNumber(input)
    this.isValidStepsPerStride(stepsPerStride)
    return stepsPerStride
  }

  public static parseRestartResponse(input: string): boolean {
    return this.parseYesNo(input)
  }

  private static parseYesNo(input: string): boolean {
    if(input === 'y'){
      return true
    }
    else if(input === 'n'){
      return false
    }
    else {
      throw new ValidationError('\n Please respond with y or n')
    }
  }

  private static parseNumber(input: any): number {
    try {
      return parseInt(input)
    }
    catch(err) {
      throw new ValidationError('\n Input is not a valid integer')
    }
  }

  private static parseJson(input: any): any {
    try {
      return JSON.parse(input)
    }
    catch(err) {
      throw new ValidationError('\n Input is not a valid array')
    }
  }

  private static isValidArray(input: any): void {
    if(! Array.isArray(input)){
      throw new ValidationError('\n Input is not a valid array')
    }
  }

  private static onlyNumbersInArray(input: any[]): void {
    input.some((el, index ) => {
      if(typeof el != 'number') {
        throw new ValidationError(`\n Input should be an array of strings \n element number ${index + 1} (${el}) is a ${typeof el}`)
      }
    })
  }

  private static hasValidLength(input: number[]): void {
    const reason = `\n A staircase has between ${config.staircase.minFlights} and ${config.staircase.maxFlights} flights (inclusive)`
    if(input.length < config.staircase.minFlights || input.length > config.staircase.maxFlights){
      throw new ValidationError(reason)
    }
  }

  private static hasValidFlightLength(staircaseDefinition: number[]): void {
    const msgPart = `\n A flight has between ${config.flight.minSteps} and ${config.flight.maxSteps} steps (inclusive)`
    staircaseDefinition.some((flightSteps, index) => {
      if(flightSteps < config.flight.minSteps || flightSteps > config.flight.maxSteps){
        throw new ValidationError(msgPart + `\n flight number ${index + 1} has ${flightSteps} steps`)
      }
    })
  }

  private static isValidStepsPerStride(stepsPerStride: number): void {
    const msgPart = `\n The steps per stride is only legal between ${config.stride.minSteps} and ${config.stride.maxSteps} (inclusive)`
    if(stepsPerStride < config.stride.minSteps || stepsPerStride > config.stride.maxSteps){
      throw new ValidationError(msgPart + `\n input was ${stepsPerStride}`)
    } else if(isNaN(stepsPerStride)){
      throw new ValidationError(msgPart + `\n input was not a valid number`)
    }
  }
}

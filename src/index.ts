import { Staircase } from "./staircase"
import { Interrogator } from "./interrogator"

// Possible OO implementatation where I assume that a staircase is something that can be reused
// while that makes less sense for the steps per stride (depends on the person)

const main = async (): Promise<void> => {
  let again = true;
  while(again){
    const staircaseDefinition: number[] = await Interrogator.askStaircase()
    const staircase = new Staircase(staircaseDefinition)
    const steps: number = await Interrogator.askStepsPerStride()
    const result = staircase.calculateSteps({stepsPerStride: steps})

    console.log(`You will need ${result} strides`)
    again = await Interrogator.askAgain()
  }
  process.exit(0)
}


main()
  .catch((err): void => console.log('Unexpected error:', err))

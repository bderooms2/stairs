interface Config {
    staircase: {
      minFlights: number,
      maxFlights: number
    },
    flight: {
      minSteps: number,
      maxSteps: number
    }
    landing: {
      stridesToTurn: number
    },
    stride: {
      minSteps: number,
      maxSteps: number
    }
}


const config: Config = {
  staircase: {
    minFlights: 1,
    maxFlights: 50
  },
  flight: {
    minSteps: 5,
    maxSteps: 30
  },
  landing: {
    stridesToTurn: 2
  },
  stride: {
    minSteps: 2,
    maxSteps: 5
  }
}

export default config

# Compile in watch mode
tsc --watch

# Run
npm start

# Run with debug logs
EXPORT DEBUG=*
node build/local/index.js

# Test
npm test

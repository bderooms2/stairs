import * as chai from 'chai'
import {Validator, ValidationError} from './../src/validator'

const assert = chai.assert

describe('Validation', () => {
  describe('Staircase', () => {
    it('throw validation error on empty input', () => {
      assert.throw(() => Validator.parseAndValidateStaircase(''), ValidationError)
    })

    it('throw validation error on non parseable input', () => {
      assert.throw(() => Validator.parseAndValidateStaircase('[15[]'), ValidationError)
    })

    it('throw validation error on wrong types', () => {
      assert.throw(() => Validator.parseAndValidateStaircase('[15,"1"]'), ValidationError)
    })

    it('throw validation error on wrong types', () => {
      assert.throw(() => Validator.parseAndValidateStaircase('{ "test": "test" }'), ValidationError)
    })

    it('throw validation error on illegal values', () => {
      assert.throw(() => Validator.parseAndValidateStaircase('[0, -1]'), ValidationError)
    })

    it('throw validation error on illegal length', () => {
      assert.throw(() => Validator.parseAndValidateStaircase('[]'), ValidationError)
    })
  })

  describe('Flight', () => {
    it('throw validation error on non numeric input', () => {
      assert.throw(() => Validator.parseAndValidateStepsPerStride(''), ValidationError)
    })
    it('throw validation error on illegal values (negative)', () => {
      assert.throw(() => Validator.parseAndValidateStepsPerStride('-1'), ValidationError)
    })

    it('throw validation error on illegal values (too big)', () => {
      assert.throw(() => Validator.parseAndValidateStepsPerStride('10'), ValidationError)
    })

    it('throw validation error on non decimal values', () => {
      assert.throw(() => Validator.parseAndValidateStaircase('4.2'), ValidationError)
    })
  })

  describe('Restart', () => {
    it('throw validation error on illegal values', () => {
      assert.throw(() => Validator.parseAndValidateStepsPerStride('sdfsdfsdf'), ValidationError)
    })
  })
})

import * as chai from 'chai'
import {Staircase} from './../src/staircase'

const assert = chai.assert

describe('Calculation', () => {
  it('Simple 1 flight calculation', () => {
    const staircase = new Staircase([15])
    const result = staircase.calculateSteps({stepsPerStride: 2})
    assert.equal(result, 8)
  })

  it('Simple 2 flight calculation', () => {
    const staircase = new Staircase([15, 15])
    const result = staircase.calculateSteps({stepsPerStride: 2})
    assert.equal(result, 18)
  })

  it('Complex calculation', () => {
    const staircase = new Staircase([5,11,9,13,8,30,14])
    const result = staircase.calculateSteps({stepsPerStride: 3})
    assert.equal(result, 44)
  })
})
